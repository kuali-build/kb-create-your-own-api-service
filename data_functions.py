# Standard Libs
import os
from os import getenv
from os.path import dirname, abspath
import json
import re
import asyncio
import shutil
from io import BytesIO
from datetime import datetime
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import encoders

# External Libs
import httpx
from loguru import logger
from fastapi import HTTPException
import pandas as pd
from cachetools import TTLCache
from dotenv import load_dotenv
import uvloop

# Assistive modules
current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)

from data_structures import Kuali, KualiQueries, KualiMutations
from mongodb_setup import initialize_mongodb, create_mongodb_index

load_dotenv()

# MongoDB Configuration
DOMAIN = getenv("DOMAIN")
PORT = int(getenv('MONGO_PORT'))
database_name = 'TheDatabase'
cache = TTLCache(maxsize=1000, ttl=300)


limits = httpx.Limits(max_keepalive_connections=5, max_connections=10)
auth_token = getenv('AUTH_TOKEN')
call_back_url = getenv('CALL_BACK_URL')
pattern = re.compile(r'\s*\([^)]*\)')

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


async def download_file(temporaryUrl: str, filename: str):
    base_url = "https://duke.kualibuild.com"
    download_url = f"{base_url}{temporaryUrl}"
    headers = {"Authorization": auth_token}

    async with httpx.AsyncClient(limits=limits, http2=True, timeout=30.0, follow_redirects=True) as client:
        async with client.stream("GET", download_url, headers=headers) as response:
            if response.status_code == 200:
                file_content = b"".join([chunk async for chunk in response.aiter_bytes()])
                logger.info(f"Name of user's file: {filename}")
                return file_content, filename
            else:
                logger.error(
                    f"Error downloading file: {response.text}, status code: {response.status_code}")
                return None, None


async def process_request(request):
    request_data = await request.json()
    documentId = request_data.get('documentId')
    callbackUrl = f"{call_back_url}/{documentId}/"

    if documentId is None:
        raise HTTPException(
            status_code=400, detail="No DOCUMENTS componentId found in the request data")

    url = Kuali.url()
    headers = Kuali.headers()
    payload = {
        "query": KualiMutations.ExportDocument(),
        "variables": {
            "documentId": documentId,
            "options": ["document", "history", "comments"],
            "timeZone": "America/New_York",
            "callbackUrl": callbackUrl,
        },
    }

    timeout = httpx.Timeout(30.0, connect=30.0)
    async with httpx.AsyncClient(limits=limits, http2=True, timeout=timeout) as client:
        response = await client.post(url, headers=headers, json=payload)
        if response.status_code == 200:
            return {"status": "ok"}


async def send_doc_to_user_api(documentId: str, url: str, jobId: str):
    url2 = Kuali.url()
    headers2 = Kuali.headers()
    payload2 = {
        "query": KualiQueries.GetDocumentData(),
        "variables": {
            "documentId": documentId
        },
    }

    async with httpx.AsyncClient(limits=limits, http2=True) as client:
        response = await client.get(url)
        logger.info(f"Amazon's Aweful Web Services url: {url}")
        response.raise_for_status()
        data_response = await client.post(url2, headers=headers2, json=payload2)
        data_response.raise_for_status()
        data_response_content = data_response.json()
        document_data = data_response_content['data']['document']['data']

    download_tasks = []
    for key in document_data:
        if 'temporaryUrl' in document_data[key]:
            temporaryUrls = document_data[key]['temporaryUrl']
            if isinstance(temporaryUrls, list):
                for tempUrl in temporaryUrls:
                    download_tasks.append(download_file(
                        tempUrl, f"{document_data[key]['filename']}_{temporaryUrls.index(tempUrl)}"))
            else:
                download_tasks.append(download_file(
                    temporaryUrls, document_data[key]['filename']))

    downloaded_files = await asyncio.gather(*download_tasks)

    for file_content, filename in downloaded_files:
        if filename.endswith(".xlsx") or filename.endswith(".csv"):
            df = pd.read_excel(BytesIO(file_content)) if filename.endswith(".xlsx") else pd.read_csv(BytesIO(file_content))
            df = df.astype(str).where(pd.notnull(df), None)
            original_columns = df.columns.tolist()
            logger.info(original_columns)
            df.columns = [f'{i+1}' for i in range(len(df.columns))]
            num_duplicate_rows_before = df.duplicated().sum()
            logger.info(f"Number of duplicate items removed: {num_duplicate_rows_before}")
            df.drop_duplicates(inplace=True)
            df.to_excel(filename) if filename.endswith(".xlsx") else df.to_csv(filename, index=False)

            if df.empty:
                continue

        client, database, collection = initialize_mongodb(
            DOMAIN, PORT, database_name, filename)

        records = df.to_dict('records')
        logger.info(f"Data Passkey Generated: {documentId}")
        for record in records:
            record['k'] = documentId

        collection.insert_many(records)
        send_to_endpoint = {
            "App Name": data_response_content['data']['document']['app']['name'],
            "Display Name": pattern.sub('', data_response_content['data']['document']['meta']['createdBy']['displayName']).strip(),
            "Email": data_response_content['data']['document']['meta']['createdBy']['email'],
            "Duid": data_response_content['data']['document']['meta']['createdBy']['schoolId'],
            "Passkey": documentId
        }

        document_content = response.content
        user_name = send_to_endpoint['Display Name']
        files = {
            'file': (f'{user_name}.pdf', document_content, 'application/pdf'),
            'document_data': (None, json.dumps(send_to_endpoint).encode(), 'application/json')
        }

        email_user = "Kuali Integration Builder <noreply@kuali.duke.edu>"
        email_send = send_to_endpoint['Email']
        subject = 'Your integration has been created and is ready for use'

        msg = MIMEMultipart()
        msg['From'] = email_user
        msg['To'] = email_send
        msg['Subject'] = subject

        headers_dict = dict(zip(original_columns, df.columns.tolist()))
        logger.info(headers_dict)
        headers_df = pd.DataFrame(list(headers_dict.items()), columns=[
                                  f"{filename} headers", "integration headers"])
        headers_html = headers_df.to_html(index=False)

        body = f"""
        <html><head><style> 
        table {{
            margin-left: auto;
            margin-right: auto;
        }}
        th, td {{
            border: 1px solid black;
            padding: 10px;
            text-align: center;
        }}
        </style></head><body>
        <p>Hello,</p>
        <p>Attached to this email, you will find a copy of your submission in PDF format as well as any 
        additional documents you uploaded. 
        These have been entered into our database and will be used to generate your unique integration in 
        Kuali Build.</p>
        <p>To access this integration, please select the 'Personalized Integration' option from the advanced 
        dropdown section in the Kuali Build form.</p>
        <p>Passkey: {send_to_endpoint['Passkey']}</p>
        <p>Your Passkey is a unique identifier that links your form submission to your personalized integration. 
        Please keep this Passkey safe, as it is the only way to access your specific dataset. It is essential 
        for accessing and managing your data, and without it, you will not be able to retrieve your information.</p>
        <p>Please note that your data is secured and can only be accessed by individuals who have this Passkey. 
        Therefore, only share this Passkey with trusted individuals who require access to this data.</p>
        <p>Below, you will find a mapping of the original column headers to their new identifiers:</p>
        {headers_html}
        <p>If you have any further questions or need additional support, please reach out using the link below:</p>
        <p><a href='https://duke.kualibuild.com/app/builder/app/62582c3688380fc89306d7b5/run' target='_blank'>
        OIT - Kuali Implementation Request</a></p>
        <p>Thank you.</p>
        <p>- OIT Kuali Support Team</p>
        </body>
        </html>
        """

        msg.attach(MIMEText(body, 'html'))

        pdf_file_path = os.path.join(current_dir, f"{user_name}.pdf")
        with open(pdf_file_path, 'wb') as f:
            f.write(document_content)

        filename = pdf_file_path
        attachment = open(filename, 'rb')

        part = MIMEBase('application', 'octet-stream')
        part.set_payload((attachment).read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition',
                        "attachment; filename= " + filename)

        msg.attach(part)

        text = msg.as_string()
        server = smtplib.SMTP('smtp.duke.edu')

        server.sendmail(email_user, email_send, text)
        server.quit()
        logger.info(f"Message: {files['document_data']}")

        for _, filename in downloaded_files:
            destination_path = os.path.join(current_dir, filename)
            if os.path.exists(destination_path):
                os.remove(destination_path)
            if filename.endswith(".xlsx"):
                shutil.move(filename, current_dir)
            if filename.endswith(".csv"):
                shutil.move(filename, current_dir)
            if filename.endswith(".pdf"):
                shutil.move(filename, current_dir)
    return {"status": "success"}
