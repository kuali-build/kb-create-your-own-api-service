# Kuali Build Self Service Integrations

[![pipeline status](https://gitlab.oit.duke.edu/kuali-build/kb-create-your-own-api-service/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/kuali-build/kb-create-your-own-api-service/-/commits/main)

***
kb-create-your-own-api-service
***

This project provides Python scripts to process, download, and store documents using Kuali's API and MongoDB. It uses FastAPI to create a service that handles requests, downloads the requested documents, and stores them in a MongoDB database. Furthermore, it provides a mechanism to send processed documents to a user-specified endpoint (not enabled at this time - but ready for external endpoint if feature is requested).<br>

## Project Architecture

This project includes instructions for three containers: - <br>

1. web-kb-create-your-own-api-service<br>
2. caddy-kb-create-your-own-api-service<br>
3. kb-create-your-own-api-service-db-1<br>

## Project Structure

The primary project file, `endpoints.py`, contains the general framework/setup, middleware, routes, and requested business logic. It leverages the FastAPI framework for API creation and the Pydantic library for data validation.

## File tree

```bash
.
├── data_functions.py
├── data_structures.py
├── delete_uploads.py
├── docker-compose.yml
├── Dockerfile
├── endpoints.py
├── mongodb_setup.py
├── read_uploads.py
└── requirements.txt
```

1 directory, 10 files

- `data_functions.py`: Handles requests for data from the Kuali system, processes received data, performs post-processing tasks such as data deduplication, database insertion, and email notifications.<br>
- `data_structures.py`: Contains information and functionality related to the Kuali platform, specifically HTTP requests to Kuali's GraphQL API.<br>
- `delete_uploads.py`: Offers two asynchronous functions for MongoDB collection deletion operations via a FastAPI web application.<br>
- `endpoints.py`: Main script that provides six RESTful APIs to interact with a MongoDB database.<br>
- `mongodb_setup.py`: Establishes a connection to MongoDB, creates the specified database and collection if they don't exist, creates a text index on the 'password' field, retrieves and logs database and collection statistics, and returns the MongoClient instance, the database, and the collection<br>
- `read_uploads.py`: Fetches data from the MongoDB collection based on provided query parameters.<br>

## Prerequisites

Required Python Libraries<br>

- `cachetools`: Provides memoization and caching utilities for multiple requests.<br>
- `fastapi`: FastAPI is the web framework for building this application.<br>
- `httptools`: httptools is a Python binding for nodejs HTTP parser, which improves our current HTTP protocol parsing.<br>
- `httpx`: HTTPX is a fully featured HTTP client for Python, allowing us speed + easy redirect gains.<br>
- `httpx[http2]`: Provides HTTP/2 support where we need it most (multiplexing and concurrency processes).<br>
- `loguru`: A logging library that makes logging easier for us.<br>
- `pandas`: Used for data manipulation, specifically turning incoming xlsx/csv files into a structured json body.<br>
- `pydantic`: Data validation.<br>
- `pymongo`: To onnect to MongoDB, interact with it, and compress our data.<br>
- `python-dotenv`: Environment variables.<br>
- `pytz`: Cross-platform timezone calculations.<br>
- `uvicorn`: ASGI server that serves your application while it runs in an event loop.<br>
- `uvloop`: Uvloop is a fast and further improve performance of our api calls.<br>
- `orjson`: Fastest Python library for JSON and is more correct than the standard json library or other third-party libraries (no brainer).<br>
- `python-multipart`: python-multipart is a library that parses HTTP multipart/form-data requests from Kuali Build's AWS (Aweful Web Services) - jk.. Amazon Web Services.<br>
- `zstandard`: Zstandard (or zstd) is a fast lossless compression algorithm and data compression tool.<br>


Before running the application, the following programs need to be installed:

- Docker: Docker is used to containerize and run the services. Download it from [here](https://docs.docker.com/desktop/install/linux-install/) and follow the installation instructions provided.<br>
- Docker Compose: Docker Compose is used to define and run multi-container Docker applications. Docker Compose comes bundled in Docker Desktop for Windows and Mac. For Linux systems, you may need to download it separately from [here](https://docs.docker.com/desktop/install/linux-install/).<br>
- Caddy Server: Caddy is used as a reverse proxy for the application. It automatically enables HTTPS for the services. Install it from [here](https://caddyserver.com/docs/install#debian-ubuntu-raspbian).<br>
- Git (Optional): Git is used to clone the application code from the repository. If you don't have Git installed, you can also download the code directly from the repository as a zip file. If you wish to install Git, download it from [here](https://git-scm.com/downloads).<br>

Docker engine:
```sh
sudo apt-get update \
    && sudo apt-get install docker.io docker-doc docker-compose podman-docker containerd runc
```

** Please note this repository was built using the following versions:

- Docker Compose version v2.18.1

```text
Client: Docker Engine - Community
 Version:           24.0.2

Server: Docker Engine - Community
 Engine:
  Version:          24.0.2

 containerd:
  Version:          1.6.21

 runc:
  Version:          1.1.7

 docker-init:
  Version:          0.19.0
```

## Docker Compose

1. The `web` service:
    - The Docker image is built from the current directory, tagged as `kuali-kb-create-your-own-api-service`.<br>
    - The container is named `web-kb-create-your-own-api-service`.<br>
    - The container exposes port 8102 to ahe host at port 0.0.0.0:8102.<br>
    - Two volumes are mounted to the container. The first volume mounts the `.env` configuration file from the host to the `/app/.env` path in the container. The second volume mounts the log directory from the host to the `/app/log/` path in the container.<br>
    - The container will be restarted automatically unless it is explicitly stopped.<br>
    - The container is connected to the `caddy_network` network.<br>

2. The `caddy` service:
    - The image used is the latest Caddy server image.<br>
    - The container is named `caddy-kb-create-your-own-api-service`.<br>
    - The container is set to restart unless stopped (for maintenance and trouble-shooting).<br>
    - Three volumes are mounted to the container. The first volume mounts the `Caddyfile` from the host to `/etc/caddy/Caddyfile` in the container. The second volume mounts the `/etc/caddy` directory from the host to `/etc/caddy` in the container. The third volume mounts the Docker socket, allowing Caddy to interact with Docker.<br>
    - The container exposes ports 80, 443, and 443/udp to the host at ports 1032, 1033, and 1033 respectively.<br>
    - The container is connected to the `caddy_network` network.<br>

3. The `db` service:
    - The image used is the latest MongoDB server image.<br>
    - The container is named `kb-create-your-own-api-service-db-1`.<br>
    - The container is set to restart unless stopped (for maintenance and trouble-shooting).<br>
    - One volume is mounted to the container. This volume mounts the database to disk on the host machine, allowing MongoDB to persist the database between startups and shutdowns.<br>
    - The container exposes ports 27018 to 27017.<br>
    - The container is connected to the `caddy_network` network.<br>

4. The `caddy_network` network:
    - On this dev machine, I had created a docker network named `caddy_network` with the bridge driver.<br>

The configuration allows our two services to communicate with each other over the caddy network. In short, our web service runs the FastAPI application, while the caddy service runs a Caddy server that acts as a reverse proxy for this project.  The other benefit of Caddy is the use of automatically obtained/renewed SSL/TLS certificates from Let's Encrypt, and also handles OCSP stapling.  Caddy also uses modern protocols and technologies like HTTP/2 and QUIC to improve both speeds + reliability of our applications (speed increase of nearly 6x HTTP/1.1 in my testing).<br>

You can learn more about caddy and how awesome it is at https://caddyserver.com/docs/<br>

- Please note that you will need to create your own `/etc/caddy/Caddyfile`, `/etc/caddy`, and `/srv/persistent-data/app/kb-create-your-own-api-service/.env` files and directories on your host as they are required by the Docker configuration.

## Installation<br>

1. Clone the repository to your server or linux/unix-based system.<br>
2. Navigate to the project directory in a terminal.<br>
3. Build the Docker image using the following command:<br>

```bash
docker-compose build
```

## Usage

- To test the application in a Docker container, use the following command:

```bash
docker-compose up
```

- To shut down this test, use the following command:

```bash
docker-compose down
```

- To run the application in the background, use the following command:

```bash
docker-compose up -d
```