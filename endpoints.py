# Standard Libs
import sys
import os
from os import getenv
from os.path import dirname, abspath
import pwd
import json
import time
import socket
from datetime import datetime, timedelta
import pytz
from typing import Optional, List
import asyncio
from asyncio import gather, all_tasks, current_task
from tempfile import gettempdir
import fcntl

# External Libs
from loguru import logger
import aiofiles
from pymongo import MongoClient, ASCENDING, errors, TEXT
import uvloop
import httptools
from dotenv import load_dotenv
from fastapi import FastAPI, UploadFile, File, Depends, HTTPException, Request
from fastapi.responses import ORJSONResponse
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from starlette.requests import Headers
from cachetools import TTLCache


current_dir = dirname(abspath(__file__))
parent_dir = dirname(current_dir)
sys.path.append(current_dir)

# Import modules
from read_uploads import process_read_data
from delete_uploads import process_delete_data, process_delete_all
from data_structures import Kuali, KualiMutations, KualiQueries
from data_functions import send_doc_to_user_api, process_request
from mongodb_setup import initialize_mongodb, create_mongodb_index

log_file = os.path.join(current_dir, 'log', 'kb-create-your-own-api-service.log')
logs = logger.add(
    log_file,
    format="{time:YYYY-MM-DD at HH:mm:ss} {exception} | {level} | {message} | {line} | {module}",
    level="INFO",
    enqueue=True
)

HOST = "0.0.0.0"
SERVE_PORT = 8102
ROOT_PATH = "/kbcreateyourownapiservice"

security = HTTPBasic()
uvloop.install()
load_dotenv()

DOMAIN = getenv("DOMAIN")
PORT = int(getenv('MONGO_PORT'))
database_name = 'TheDatabase'
data_values = 'TheDatabase'
client, database, collection = initialize_mongodb(DOMAIN, PORT, database_name, data_values)
cache = TTLCache(maxsize=100, ttl=300)

app = FastAPI(
    openapi_url="/openapi.json",
    docs_url="/docs",
    redoc_url="/redocs",
    default_response_class=ORJSONResponse
)


def verify_credentials(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = getenv('BASIC_AUTH_USERNAME')
    correct_password = getenv('BASIC_AUTH_PASSWORD')
    if credentials.username != correct_username or credentials.password != correct_password:
        raise HTTPException(
            status_code=401,
            detail="Incorrect username or password"
        )
    return credentials


def process_headers(headers: Headers) -> dict:
    required_headers = ["referer", "x-kuali-origin", "x-kuali-user-id", "x-kuali-user-ssoid"]
    return {header: headers.get(header) for header in required_headers}


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    headers = process_headers(request.headers)
    referer = headers.get('referer') or f"{headers['x-kuali-origin']} | {headers['x-kuali-user-id']} | {headers['x-kuali-user-ssoid']}"
    query_string = request.query_params._list

    start_time = time.time()
    try:
        response = await call_next(request)
        process_time_str = str(timedelta(seconds=time.time() - start_time))
        response.headers["X-Process-Time"] = process_time_str

        log_msg = f"{referer} | {query_string} | {process_time_str} | {request.url.path} | {response.status_code}"
        logger.success(log_msg)
        return response
    except Exception as dammit:
        error_msg = f"{referer} | {query_string} | |{request.url.path} | | {str(dammit)}"
        logger.error(error_msg)
        raise dammit


async def cleanup_logs(days=14):
    now = datetime.now(pytz.timezone('US/Eastern'))
    formatted_time = now.strftime("%Y-%m-%d")
    cutoff = now - timedelta(days=days)
    cutoff = cutoff.replace(tzinfo=None)

    async with aiofiles.open(log_file, mode='r') as f:
        lines = await f.readlines()

    new_lines = []
    for line in lines:
        elements = line.split(" at ", 1)
        log_time_str = " ".join(elements[:1])
        try:
            log_time = datetime.strptime(log_time_str, "%Y-%m-%d")
        except ValueError:
            continue

        if log_time > cutoff:
            new_lines.append(line)

    async with aiofiles.open(log_file, mode='w') as f:
        await f.write(''.join(new_lines))


# Create a lock file so workers do not duplicate one another
LOCKFILE = os.path.join(gettempdir(), 'Workers.lock')


@app.on_event("startup")
async def startup_event():
    with open(LOCKFILE, 'w') as f:
        try:
            fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            return  # Another instance has the lock, we should exit.

        # If we reach here, we have the lock and can safely execute the startup tasks.
        await cleanup_logs()
        logger.info("Logs scrubbed")
    uid = os.getuid()
    username = pwd.getpwuid(uid).pw_name
    hostname = socket.gethostname()
    logger.info(f"Initializing on host {hostname}")
    logger.info(f"Parent directory is {parent_dir}")
    logger.info(f"Current directory is {current_dir}")
    logger.info(f"User is {username}")
    logger.success(f"Application Started on {HOST}:{PORT}")
    logger.info(f"Route: {ROOT_PATH}/*")


@app.on_event("shutdown")
async def shutdown_event():
    tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]
    [task.cancel() for task in tasks]
    try:
        await asyncio.gather(*tasks, return_exceptions=False)
    except asyncio.exceptions.CancelledError:
        logger.opt(exception=True).debug("A task was cancelled")
    logger.success(f"Cancelled {len(tasks)} outstanding tasks")
    logger.remove(logs)


@app.post("/call/kuali", response_class=ORJSONResponse, dependencies=[Depends(verify_credentials)],
          tags=["OIT - Initiate Kuali Build Document Collection"], summary="Wake up endpoint")
async def run_graphql_request(request: Request):
    return await process_request(request)


@app.get("/callback/{documentId}/", response_class=ORJSONResponse, dependencies=[Depends(verify_credentials)],
         tags=["OIT - Grab Document and send to user requested endpoint"], summary="Retrieve document")
async def document_exported(jobId: str, url: str, documentId: str):
    return await send_doc_to_user_api(documentId, url, jobId)


@app.get("/data/", response_class=ORJSONResponse, dependencies=[Depends(verify_credentials)], tags=["Kuali Build your own API"])
async def read_data(password: str, field: Optional[str] = None, value: Optional[str] = None, dropdown_field: Optional[str] = None, default_dropdown_value: Optional[str] = ''):
    response = await process_read_data(password, collection, cache, field, value, dropdown_field, default_dropdown_value)
    return ORJSONResponse(response)


@app.delete("/data/", response_class=ORJSONResponse, dependencies=[Depends(verify_credentials)],
            tags=["Kuali Build your own API"])
async def delete_data(field: Optional[str] = None, value: Optional[str] = None):
    response = await process_delete_data(field, value, collection, cache)
    return response


@app.delete("/deleteall/", response_class=ORJSONResponse, dependencies=[Depends(verify_credentials)],
            tags=["Kuali Build your own API"])
async def delete_all():
    response = await process_delete_all(collection, cache)
    return response


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(
        __name__ + ":app",
        host=HOST,
        port=SERVE_PORT,
        proxy_headers=True,
        reload=True,
        root_path=ROOT_PATH,
        loop="uvloop",
        http="httptools",
        workers=2,
        log_level="info",
        timeout_keep_alive=int(os.environ.get("KEEP_ALIVE", 60)),
        limit_max_requests=int(os.environ.get("MAX_REQUESTS", 1000)),
    )
