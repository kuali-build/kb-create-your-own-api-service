# Standard Libs
from typing import Optional

# External Libs
from fastapi import HTTPException
from fastapi.responses import ORJSONResponse


async def process_delete_data(field: str, value: str, collection, cache):
    if not field or not value:
        raise HTTPException(
            status_code=400, detail="Missing field or value for delete operation."
        )
    result = collection.delete_many({field: value})
    cache.clear()
    return ORJSONResponse({"message": f"{result.deleted_count} document(s) were deleted."})


async def process_delete_all(collection, cache):
    result = collection.delete_many({})
    cache.clear()
    return ORJSONResponse({"message": f"{result.deleted_count} document(s) were deleted."})
