import os
from os import getenv
from os.path import dirname, abspath
import re
import hashlib
import json
from pymongo import MongoClient, TEXT, errors
from bson import json_util
from loguru import logger
from dotenv import load_dotenv
import zstandard

load_dotenv()

DOMAIN = getenv("MONGO_HOST")
PORT = int(getenv("MONGO_PORT"))
database_name = 'TheDatabase'
collection_name = 'TheDatabase'

def create_mongodb_index(collection, index_fields):
    try:
        collection.create_index(index_fields)
    except errors.OperationFailure as e:
        logger.error(f"Could not create text index: {e}")

def hash_doc(doc):
    doc_copy = doc.copy()
    doc_copy.pop('_id', None)
    doc_json = json.dumps(doc_copy, default=json_util.default, sort_keys=True)
    return hashlib.md5(doc_json.encode('utf-8')).hexdigest()


def remove_duplicates(collection):
    cursor = collection.find({})
    hashes = {}
    duplicates_count = 0

    for doc in cursor:
        hashed_doc = hash_doc(doc)
        if hashed_doc in hashes:
            collection.delete_one({'_id': doc['_id']})
            duplicates_count += 1
        else:
            hashes[hashed_doc] = doc['_id']

    logger.info(f"Removed {duplicates_count} duplicate document(s).")


def initialize_mongodb(DOMAIN, PORT, database_name, filename):
    client = MongoClient(f'mongodb://{DOMAIN}:{PORT}/', compressors='zstd')
    # client = MongoClient(DOMAIN, PORT, compressors='zstd')
    logger.info(client)

    db = client[database_name]

    collection_name = re.sub(r'\W+', '', filename).lower().replace("_", "")

    if collection_name not in db.list_collection_names():
        db.create_collection(collection_name, storageEngine={
            'wiredTiger': {
                'configString': 'block_compressor=zstd'
            }
        })

    db.command('compact', collection_name)

    collection = db[collection_name]

    index_fields = [("password", TEXT)]
    create_mongodb_index(collection, index_fields)
    remove_duplicates(collection)

    server_version = client.server_info()["version"]
    database_names = client.list_database_names()

    db_stats = db.command("dbstats")
    db_size_mb = db_stats['dataSize'] / (1024 * 1024)
    logger.info(f"Database size: {db_size_mb} MB")

    total_docs = 0
    for coll_name in db.list_collection_names():
        coll = db[coll_name]
        coll_stats = db.command('collstats', coll_name)
        coll_size_mb = coll_stats['size'] / (1024 * 1024)
        doc_count = coll.count_documents({})
        total_docs += doc_count
        logger.info(f"Collection '{coll_name}' size: {coll_size_mb} MB, Documents: {doc_count}")

    collection_infos = db.list_collection_names()
    logger.info(f"Collection Info: {collection_infos}")

    logger.info(f"Total documents in the database: {total_docs}")
    logger.info(f"Server version: {server_version}")
    logger.info(f"Databases: {database_names}")

    return client, db, collection
