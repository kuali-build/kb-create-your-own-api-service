# Standard Libs
from os import getenv
from pydantic import BaseModel

# External Libs
from dotenv import load_dotenv

load_dotenv()


class Kuali:
    @staticmethod
    def url():
        return "https://duke.kualibuild.com/app/api/v0/graphql"

    @staticmethod
    def headers():
        return {
            "Accept-Encoding": "gzip, deflate, br",
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Connection": "keep-alive",
            "DNT": "1",
            "Origin": "https://duke.kualibuild.com",
            "Authorization": f'{getenv("AUTH_TOKEN")}',
        }


class KualiMutations:
    @staticmethod
    def ExportDocument():
        return """
mutation ExportDocument($documentId: ID!, $callbackUrl: String!, $options: [String!]!, $timeZone: String) {
exportDocument(
id: $documentId
callbackUrl: $callbackUrl
options: $options
timeZone: $timeZone
)
}
"""


class KualiQueries:
    @staticmethod
    def GetDocumentData():
        return """
query getDocument($documentId: ID!) {
document(id: $documentId, keyBy: ID) {
meta
app {
name
}
id
data
}
}
"""
