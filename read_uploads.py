from fastapi import HTTPException
from fastapi.responses import ORJSONResponse
from bson import ObjectId
from bson.regex import Regex
from loguru import logger
from typing import Optional

async def process_read_data(password: str, collection, cache, field: Optional[str] = None, 
                            value: Optional[str] = None, dropdown_field: Optional[str] = None, 
                            default_dropdown_value: Optional[str] = ''):
    if not password:
        raise HTTPException(
            status_code=400,
            detail="Missing password for data retrieval."
        )

    cache_key = (password, field, value)
    if cache_key in cache:
        return cache[cache_key]

    query = {"k": password}

    if field and value:
        query[field] = {"$regex": value, "$options": "i"}
    elif value:
        query["$or"] = [{str(i+1): {"$regex": value, "$options": "i"}} for i in range(15)] 

    data = collection.find(query)
    
    res = list(data)
    for record in res:
        for key, value in record.items():
            if isinstance(value, ObjectId):
                record[key] = str(value)
        if dropdown_field and dropdown_field in record:
            record['dropdown'] = record[dropdown_field]
        else:
            record['dropdown'] = default_dropdown_value

    cache[cache_key] = res
    return res
